<?php

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
    
            <div class="container">
                <div class="teacher-entry">                                        
                    <?php if(have_posts()):
                        while(have_posts()) : the_post(); ?>             
                            <figure class="teacher-entry__featured col-12 col-md-10 col-lg-5">
                                <?php the_post_thumbnail('featured-teacher'); ?>
                            </figure>
                            <div class="teacher-entry__content-wrapper col-12 col-md-10 col-lg-6">
                                <div class="teacher-entry__content-header">
                                    <h2 class="teacher-entry__title"><?php the_title(); ?></h2>  
                                    <?php $teacher_role = get_field('teacher_role', $post_ID); ?>
                                    <?php if ($teacher_role) : ?>
                                        <h5 class="teachers__teacher-role"><?= $teacher_role; ?></h5>
                                    <?php endif; ?> 
                                </div>                     
                                <div class="teacher-entry__content"><?php the_content(); ?></div>
                            </div>

                        <?php
                        endwhile;
                    endif;
                
                    wp_reset_postdata(); 
                    ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();
