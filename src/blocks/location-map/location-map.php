<?php
$section_title = get_field('section_title');
?>

<section  <?php ign_block_attrs( $block, 'location-map' ); ?>> 
    <div class="container">
        <div class="location-map__wrapper">
            <?php if( !empty($section_title)): ?>
                <h2 class="location-map__section-title"><?= $section_title; ?></h2>
            <?php endif; ?>

            <?php if( have_rows('location') ): ?> 
                <?php while ( have_rows('location') ) : the_row(); ?>
        
                    <div class=location-map__location>
                        <?php $address = get_sub_field('address'); ?>
                        <?php $address_description = get_sub_field('address_description'); ?>
                        <?php $map_desctop = get_sub_field('map_desctop'); ?>
                        <?php $map_mobile = get_sub_field('map_mobile'); ?>

                        <div class=location-map__address-wrapper>
                            <?php if( !empty($address)): ?>  
                                <p class="location-maps__address"><?= $address;?></p>  
                            <?php endif; ?>

                            <?php if( !empty($address_description)): ?>  
                                <p class="location-maps__address-description"><?= $address_description;?></p>  
                            <?php endif; ?>
                        </div>    

                        <?php if( !empty($map_desctop)): ?>  
                            <figure class="location-maps__mmap-desctop d-none d-lg-block"><?= get_image($map_desctop, 'map_desctop');?></figure>  
                        <?php endif; ?>

                        <?php if( !empty($map_mobile)): ?>  
                            <figure class="location-maps__mmap-mobile d-lg-none"><?= get_image($map_mobile, 'map_mobile');?></figure>  
                        <?php endif; ?>
                    </div>    
        
                <?php endwhile; ?>             
            <?php endif; ?>

        </div>
    </div>
</section>