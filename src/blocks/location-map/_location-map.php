<?php

function location_map_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'location-map',
		'title'           => __( 'Location Map' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/location-map/location-map.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'map', 'adress' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'location_map_register_block' );
}