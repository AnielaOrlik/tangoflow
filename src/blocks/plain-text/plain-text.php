<?php
    $section_title = get_field('section_title');
    $content = get_field('content');
?>

<section  <?php ign_block_attrs( $block, 'plain-text' ); ?>>    
    <div class="container">
        <?php if( !empty($section_title)): ?>
            <h2 class="plain-text__section-title"><?= $section_title; ?></h2>
        <?php endif; ?>

        <?php if( !empty($content)): ?>
            <div class="plain-text__content"><?= $content; ?></div>
        <?php endif; ?>
    </div>
</section>