<?php

function plain_text_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'plain_text',
		'title'           => __( 'Plain Text' ),
		'description'     => __( 'Plain Text' ),
		'render_template' => 'src/blocks/plain-text/plain-text.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array('text editor' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'plain_text_register_block' );
}