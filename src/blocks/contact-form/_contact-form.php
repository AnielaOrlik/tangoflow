<?php

function contant_form_register_block () {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'contact-form',
		'title'           => __( 'Contant form' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/contact-form/contact-form.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'contant', 'form' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'contant_form_register_block' );
}
