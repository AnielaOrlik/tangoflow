<?php
$section_title = get_field('section_title');
$contact_form_shortcode = get_field('contact_form_shortcode');
?>

<section  <?php ign_block_attrs( $block, 'contact-form' ); ?>> 
    <div class="container">
        <div class="contact-form__wrapper">
            <?php if( !empty($section_title)): ?>
                <h2 class="contact-form__section-title"><?= $section_title; ?></h2>
            <?php endif; ?>

            <?php if( !empty($contact_form_shortcode)): ?>
                <div class="contact-form__form"><?php echo do_shortcode($contact_form_shortcode); ?></div>
            <?php endif; ?>
        </div>
    </div>
</section>


