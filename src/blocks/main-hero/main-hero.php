<?php
    $section_id = get_field('section_id');
    $bg_image = get_field('bg_image');
    $bg_image_mobile = get_field('bg_image_mobile');
    $slogan = get_field('slogan');
    $logo = get_field('logo'); 
    $side_panel_text = get_field('side_panel_text');
    $button = get_field('button');
?>

<section  <?php ign_block_attrs( $block, 'main-hero' ); ?> id="<?= $section_id; ?>">
    <div class="main-hero__hero-wrapper">
        <?php if( !empty($bg_image)): ?>
            <figure class="main-hero__bg-image">
                <?= wp_get_attachment_image($bg_image, 'full-width', false, ['class' => 'd-none d-md-block']); ?>
                <?php if( !empty($bg_image_mobile)): ?>
                    <?= wp_get_attachment_image($bg_image_mobile, 'full-width-mobile', false, ['class' => 'd-block d-md-none']); ?>
                <?php endif; ?>
            </figure>
        <?php endif; ?>

        <div class="container">
            <?php if( !empty($slogan)): ?>
                <h1 class="main-hero__slogan"><?= $slogan; ?></h1> 
            <?php endif; ?>

            <?php if( !empty($button)) : ?>
                    <div class="main-hero__button">
                        <?php get_theme_part('elements/button', ['button' => $buttonn, 'style' => 'secondary', 'class' => 'c-button--secondary']); ?>
                    </div>
                <?php endif; ?>
        </div>
    </div>
</section>