<?php

function main_hero_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'main_hero',
		'title'           => __( 'Main Hero' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/main-hero/main-hero.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'tile', 'icon' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'main_hero_register_block' );
}