<?php

function image_text_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'image_text',
		'title'           => __( 'Image and Text Grid' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/image-text/image-text.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'image', 'text editor' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'image_text_register_block' );
}