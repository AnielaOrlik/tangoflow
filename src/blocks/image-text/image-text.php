<?php
    $section_title = get_field('section_title');
?>

<section <?php ign_block_attrs( $block, 'image-text' ); ?>> 
    <div class="container">
        <?php if( !empty($section_title)): ?>
            <h2 class="image-text__section-title"><?= $section_title; ?></h2>
        <?php endif; ?>
               
            <?php if( have_rows('image_and_text') ): ?> 
                <?php while ( have_rows('image_and_text') ) : the_row(); ?>
                    <div class="image-text__card">
                        <?php 
                        $image = get_sub_field('image'); 
                        $title = get_sub_field('title'); 
                        $text = get_sub_field('text'); 
                        ?>

                        <?php if( !empty($image)): ?>
                            <figure class="image-text__card-image col-12 col-sm-8 col-md-8 col-lg-5 col-xxl-4"><?= get_image($image,'classes-thumb'); ?></figure>
                        <?php endif; ?>
                        <div class="image-text__card-content col-12 col-sm-8 col-md-8 col-lg-7 col-xl-6">
                            <?php if( !empty($title)): ?>
                                <h3 class="image-text__text"><?= $title; ?></h3>
                            <?php endif; ?>

                            <?php if( !empty($text)): ?>
                                <div class="image-text__text"><?= $text; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        
    </div> 
</section>