<section <?php ign_block_attrs( $block, 'logo-grid' ); ?>>
    <div class="container">
        <?php if($section_title = get_field('section_title')): ?>
            <h2 class="logo-grid__section-title"><?= $section_title; ?></h2>
        <?php endif; ?>
        <?php if( $images = get_field('logo_gallery') ): ?>
            <ul class="logo-grid__logo-gallery">
                <?php foreach( $images as $image_id ): ?>
                <li class="logo-grid__logo-gallery-item">
                    <?= get_image( $image_id); ?>
                </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>