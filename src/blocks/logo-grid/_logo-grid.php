<?php

function logo_grid_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'logo-grid',
		'title'           => __( 'Logo gallery' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/logo-grid/logo-grid.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'logo', 'gallery' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'logo_grid_register_block' );
}