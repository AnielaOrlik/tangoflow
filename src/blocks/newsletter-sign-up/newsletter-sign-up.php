<?php
$section_title = get_field('section_title');
$cover = get_field('cover');
$text = get_field('text');
$sign_up_form_shortcode = get_field('sign_up_form_shortcode');
?>


<section <?php ign_block_attrs( $block, 'newsletter' ); ?>>
    <div class="container">
        <?php if(!empty($section_title)): ?>
            <h2 class="newsletter__section-title"><?= $section_title; ?></h2>
        <?php endif; ?>
        <div class="newsletter__content">
            <div class="newsletter__content-col-1 col-10 col-md-4">
                <?php if( !empty($cover)): ?>
                    <figure class="newsletter__cover"><?= get_image($cover, 'newsletter');?></figure>
                <?php endif; ?>
            </div>
            <div class="newsletter__content-col-2 col-12 col-md-7 col-lg-5">
                <?php if(!empty($text)): ?>
                    <div class="newsletter__text"><?= $text; ?></div>
                <?php endif; ?>

                <?php if( !empty($sign_up_form_shortcode)): ?>
                    <div class="newsletter__form"><?php echo do_shortcode($sign_up_form_shortcode); ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>