<?php

function newsletter_sign_up_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'newsletter-sign-up',
		'title'           => __( 'Newsletter Sign Up Form' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/newsletter-sign-up/newsletter-sign-up.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'form', 'newsletter' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'newsletter_sign_up_register_block' );
}