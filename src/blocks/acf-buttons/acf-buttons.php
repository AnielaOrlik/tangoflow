<?php 
    $buttons = get_field('buttons') ?: [0 => ['style' => 'primary', 'button' => ['title' => 'Button', 'url' => '#', 'target' => '_self']]];
    $alignment = get_field('alignment') ?: 'left';
?>
<div <?php ign_block_attrs( $block, 'c-button__wrapper c-button__wrapper--'.$alignment ); ?>>
    <?php foreach($buttons as $btn): ?>
        <?php 
            $button = $btn['button']; 
            $style = $btn['style']; 
        ?>
        <?php get_theme_part('elements/button', ['button' => $button, 'style' => $style]); ?>
    <?php endforeach; ?>
</div>
