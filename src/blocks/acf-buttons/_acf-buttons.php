<?php

function acf_buttons_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'acf-buttons',
		'title'           => __( 'Przyciski' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/acf-buttons/acf-buttons.php',
		'category'        => 'tangoflow',
		'icon'            => 'button',
		'keywords'        => array( 'przycisk', 'button' ),
		'supports'        => array(
			'anchor' => true
		),
		'example'  => array(
			'attributes' => array(
				'mode' => 'preview',
				'data' => array(
				  'alignment' => "left",
				  'buttons' => array(
					  'style' => 'primary',
					  'button' => array(
						  'url' => '#',
						  'title' => 'Button',
						  'target' => '_self'
					  )
				  )
				)
			)
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'acf_buttons_register_block' );
}
