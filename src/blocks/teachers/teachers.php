<?php
    $section_title = get_field('section_title');
    
 ?>
<section  <?php ign_block_attrs( $block, 'teachers' ); ?>>
    <?php if( !empty($section_title)): ?>
        <h2 class="teachers__section-title"><?= $section_title; ?></h2> 
    <?php endif; ?>

    <div class="teachers__post-grid">
            <?php $args = array('post_type' => 'teacher', 'order' => 'ASC');
            
            $loop = new WP_Query( $args ); 
            
            while ( $loop->have_posts() ) : $loop->the_post(); 
            
                $post_ID = get_the_ID();
                $title = get_the_title($post_ID);
                $teacher_role = get_field('teacher_role', $post_ID);
                $thumbnail = get_post_thumbnail_id($post_ID);
                $permalink = get_the_permalink($post_ID);
                $excerpt = get_the_excerpt($post_ID);
                ?>          
                <a class="teachers__link" href="<?= $permalink; ?>">
                    <article class="teachers__card">                
                        <figure class="teachers__featured"><?= get_image($thumbnail, 'teacher-thumb'); ?></figure>
                        <div class="teachers__content">
                            <div class="teachers__content-header">
                                <?php if ($title) : ?>
                                    <h3 class="teachers__title"><?= $title; ?></h3>
                                <?php endif; ?>
                                <?php if ($teacher_role) : ?>
                                    <h5 class="teachers__teacher-role"><?= $teacher_role; ?></h5>
                                <?php endif; ?>
                            </div>
                            <?php if ($excerpt) : ?>
                                <p class="teachers__description"><?= $excerpt; ?></p>
                            <?php endif; ?>
                            <span class="teachers__more"><?= __('more', 'tangoflow'); ?></span>
                        <div>               
                    </article>     
                </a>        
            <?php    
            endwhile;
            ?>

            <?php
            wp_reset_postdata(); 
            ?>
        </div> 

</section>