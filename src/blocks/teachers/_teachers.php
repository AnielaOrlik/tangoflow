<?php

function teachers_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'teachers',
		'title'           => __( 'Teachers' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/teachers/teachers.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'text column', 'video' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'anchor' => true,
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'teachers_register_block' );
}