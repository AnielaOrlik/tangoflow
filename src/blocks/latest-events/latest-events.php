
<?php
    $section_id = get_field('section_id');
    $section_title = get_field('section_title');
    $button = get_field('button');
?>

<section  <?php ign_block_attrs( $block, 'latest-events' ); ?> id="<?= $section_id; ?>">
    <div class="container">     
        <?php if( !empty($section_title)): ?>
                <h2 class="latest-events__section-title"><?= $section_title; ?></h2> 
        <?php endif; ?>

        <div class="latest-events__post-grid">
            <?php $args = array('post_type' => 'event', 'posts_per_page' => 3);
            
            $loop = new WP_Query( $args ); 
            
            while ( $loop->have_posts() ) : $loop->the_post(); 
                get_theme_part('elements/event-card', ['post_ID' => get_the_ID()]); 
            endwhile;
            ?>

            <?php
            wp_reset_postdata(); 
            ?>
        </div> 

        <?php if( !empty($button)) : ?>
            <div class="latest-events__button">
                <?php get_theme_part('elements/button', ['button' => $button]); ?>
            </div>
        <?php endif; ?> 
    </div>
</section>