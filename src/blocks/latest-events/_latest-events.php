<?php

function latest_events_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'latest_events',
		'title'           => __( 'Latest Events' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/latest-events/latest-events.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'event', 'grid' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'latest_events_register_block' );
}