<?php

function latest_post_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'latest_post',
		'title'           => __( 'Latest Post' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/latest-post/latest-post.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'post', 'card' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'latest_post_register_block' );
}