<?php
    $title = get_field('title');
?>

<section  <?php ign_block_attrs( $block, 'latest-post' ); ?>>
    <div class="container"> 
        <div class="latest-post__heading-wrap col-12 col-md-10">    
            <?php if( !empty($title)): ?>
                    <h2 class="latest-post__title"><?= $title; ?></h2> 
            <?php endif; ?>

            <a class="latest-post__blog-link" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?= get_image('angle-right')?></a>
        </div>
        <div class="latest-post__post">
        <?php $the_query = new WP_Query( 'posts_per_page=1' ); 
            while ($the_query -> have_posts()) : $the_query -> the_post();?>
                <?php ign_template( 'card' ); ?>
            <?php endwhile;   
            wp_reset_postdata();?>
        </div> 

    </div>
</section>