<?php

function text_and_video_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'text_and_video',
		'title'           => __( 'Text and Video' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/text-and-video/text-and-video.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'text column', 'video' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'anchor' => true,
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'text_and_video_register_block' );
}