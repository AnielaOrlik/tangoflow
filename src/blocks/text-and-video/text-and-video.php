<?php
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('button');
?>

<section  <?php ign_block_attrs( $block, 'text-and-video' ); ?>>
    <div class="container">     
        <div class="text-and-video__text-col col-12 col-md-10 col-lg-8 col-xl-6">
        <?= $section_id; ?>
            <?php if( !empty($title)): ?>
                    <h2 class="text-and-video__title"><?= $title; ?></h2> 
            <?php endif; ?>

            <?php if( !empty($text)): ?>
                <p class="text-and-video__text"><?= $text; ?></p> 
            <?php endif; ?>

            <?php if( !empty($button)) : ?>
                <div class="text-and-video__button">
                    <?php get_theme_part('elements/button', ['button' => $button]); ?>
                </div>
            <?php endif; ?>
        </div>  

        
        <?php if( have_rows('video') ): ?> 
            <?php while ( have_rows('video') ) : the_row(); ?>
                <div class="text-and-video__video-col col-12 col-md-8 col-lg-6 col-xl-5">
                    <?php $thumbnail_image = get_sub_field('thumbnail_image'); ?>
                    <?php if( !empty($thumbnail_image)): ?>  
                        <figure class="text-and-video__video-btn">
                            <?= get_image($thumbnail_image, 'video-thumb'); ?>
                            <span class="text-and-video__play-icon"><?= get_image('play'); ?></span>
                        </figure>  
                    <?php endif; ?>
                </div>       
                
                <?php $video_link = get_sub_field('video_link'); ?>
                <?php if( !empty($video_link)): ?> 
                    <div class="text-and-video__video-lightbox">
                        <iframe class="text-and-video__video" width="900" height="506" src="<?= $video_link; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <button class="text-and-video__close-btn">
                                <?= get_image('close-btn'); ?>
                        </button>
                    </div>
                <?php endif; ?>   

            <?php endwhile; ?>             
        <?php endif; ?>  
    </div> 
</section>