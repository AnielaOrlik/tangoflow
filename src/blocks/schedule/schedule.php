<?php
    $section_title = get_field('section_title');
    $schedule_content = get_field('schedule_content');
?>

<section  <?php ign_block_attrs( $block, 'schedule' ); ?>>    
    <div class="container">
        <?php if( !empty($section_title)): ?>
            <h2 class="schedule__section-title"><?= $section_title; ?></h2>
        <?php endif; ?>

        <?php if( !empty($schedule_content)): ?>
            <div class="schedule__content"><?= $schedule_content; ?></div>
        <?php endif; ?>
    </div>
</section>