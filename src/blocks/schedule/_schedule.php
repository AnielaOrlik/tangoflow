<?php

function schedule_register_block() {
// Section Menu Block
	acf_register_block_type( array(
		'name'            => 'schedule',
		'title'           => __( 'Schedule' ),
		'description'     => __( '' ),
		'render_template' => 'src/blocks/schedule/schedule.php',
		'category'        => 'content-presentation',
		'mode'			  => 'preview',
		'icon'            => get_image('tangoflow'),
		'keywords'        => array( 'image', 'text editor' ),
		'align'			  => 'full',
		'supports'        => array(
			'align'  => array( 'full' ),
			'align_text' => false,
		)
	) );
}

// Check if function exists and hook into setup and adds all blocks.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'schedule_register_block' );
}