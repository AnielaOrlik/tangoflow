<?php 
    $button = $button ?: get_field('button');
    $style = $style ?: 'primary';
?>

<a href="<?= $button['url'];?>" target="<?= $button['target'];?>" class="c-button c-button--<?= $style;?>"><?= $button['title'];?></a>