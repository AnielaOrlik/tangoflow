<?php
    $title = get_the_title($post_ID);
    $thumbnail = get_post_thumbnail_id($post_ID);
    $permalink = get_the_permalink($post_ID);
    $excerpt = get_the_excerpt($post_ID);
    $datetime = get_field('event_datetime', $post_ID);
?>
<a class="event-card__link" href="<?= $permalink; ?>">
    <article class="event-card">
        <figure class="event-card__featured col-12 col-md-5 col-lg-4"><?= get_image($thumbnail, 'event-thumb'); ?></figure>
        <div class="event-card__content col-12 col-md-7 col-lg-8">
            <div class="event-card__content-header">
                <?php if ($datetime) : ?>
                    <h5 class="event-card__datetime"><?= $datetime; ?></h5>
                <?php endif; ?>
                <?php if ($title) : ?>
                    <h3 class="event-card__title"><?= $title; ?></h3>
                <?php endif; ?>
            </div>
            <?php if ($excerpt) : ?>
                <p class="event-card__description"><?= $excerpt; ?></p>
            <?php endif; ?>
            <span class="event-card__more"><?= __('learn more', 'tangoflow'); ?></span>
        <div>
    </article>
</a>