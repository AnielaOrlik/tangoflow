<?php 
    $backgroud_image_desctop = get_field('backgroud_image_desctop', 'options');
    $background_image_mobile = get_field('background_image_mobile', 'options');
    $copyright = get_field('copyright', 'options');
    $privacy_policy = get_field('privacy_policy', 'options');
    $privacy_policy_url = $privacy_policy['url'];
    $privacy_policy_title = $privacy_policy['title'];
?>

<footer id="colophon" class="main-footer" role="contentinfo">
    <?php if( $backgroud_image_desctop ) : ?>
        <figure class="main-footer__background">
            <?= wp_get_attachment_image($backgroud_image_desctop, 'full-width', false, ['class' => 'd-none d-md-block']); ?>
            <?php if($background_image_mobile) : ?>
                <?= wp_get_attachment_image($background_image_mobile, 'full-width-mobile', false, ['class' => 'd-block d-md-none']); ?>
            <?php endif; ?>
        </figure>
    <?php endif; ?>
    <div class="container">
        <div class="main-footer__top-wrapper">
            <div class="row">
                <div class="main-footer__addresses col-12 col-md-4 col-lg-4 col-xl-6">
                    <?php if( have_rows('addresses','options') ): while ( have_rows('addresses','options') ) : the_row(); 
                        $title = get_sub_field('title');
                        $address_1 = get_sub_field('address_1');
                        $address_2 = get_sub_field('address_2'); 
                    ?>    
                    <?php if( !empty($title)): ?>
                        <h4 class="main-footer__addresses-title"><?= $title; ?></h4>
                    <?php endif; ?> 

                    <?php if( !empty($address_1)): ?>
                        <p class="main-footer__addresses-address"><?= $address_1; ?></p>
                    <?php endif; ?>

                    <?php if( !empty($address_2)): ?>
                        <p class="main-footer__addresses-address"><?= $address_2; ?></p>
                    <?php endif; ?>
                    
                    <?php endwhile; endif; ?>
                </div>

                <div class="footer__contact col-12 col-md-4 col-lg-4 col-xl-3">
                    <?php if( have_rows('email_contant','options') ): while ( have_rows('email_contant','options') ) : the_row(); 
                        $title = get_sub_field('title'); 
                    ?>    
                        <?php if( !empty($title)): ?>
                            <h4 class="main-footer__email-contant-title"><?= $title; ?></h4>
                        <?php endif; ?> 
                        
                        <?php if( have_rows('email_addresses') ): while ( have_rows('email_addresses') ) : the_row(); 
                            $email = get_sub_field('email'); 
                        ?>
                            <?php if(!empty($email)): 
                                $email_url = $email['url'];
                                $email_title = $email['title'];
                            ?>
                                <a class="main-footer__email" href="<?= $email_url; ?>" ><?= $email_title; ?></a>
                            <?php endif; ?>
                        <?php endwhile; endif;
                    endwhile; endif; ?>

                    <?php if( have_rows('whatsapp_contant','options') ): while ( have_rows('whatsapp_contant','options') ) : the_row(); 
                        $title = get_sub_field('title'); 
                    ?>    
                        <?php if( !empty($title)): ?>
                            <h4 class="main-footer__whatsapp-contant-title"><?= $title; ?></h4>
                        <?php endif; ?> 
                        
                        <?php if( have_rows('phone_numbers') ): while ( have_rows('phone_numbers') ) : the_row();
                            $phone_number = get_sub_field('phone_number'); 
                        ?>    
                            <?php if(!empty($phone_number)): 
                                $phone_number_url = $phone_number['url'];
                                $phone_number_title = $phone_number['title'];
                            ?>
                                <a class="main-footer__phone-number" href="<?= $phone_number_url; ?>" ><?= $phone_number_title; ?></a>
                            <?php endif; ?>
                        <?php endwhile; endif;
                    endwhile; endif; ?>
                </div>
            
                <div class="footer__social-media col-12 col-md-4 col-lg-3">
                        <?php if( have_rows('social_media','options') ): while ( have_rows('social_media','options') ) : the_row(); 
                            $title = get_sub_field('title'); 
                        ?>    
                            <?php if( !empty($title)): ?>
                                <h4 class="main-footer__social-media-title"><?= $title; ?></h4>
                            <?php endif; ?> 
                            
                            <?php if( have_rows('social_icons') ): while ( have_rows('social_icons') ) : the_row();
                                $icon = get_sub_field('icon');
                                $link = get_sub_field('link');
                                if($icon) : ?>
                                    <a class="main-footer__social-media-icon" href="<?= $link; ?>" target="_blank"><?= get_image($icon); ?></a>
                            <?php endif; ?>
                            <?php endwhile; endif;
                        endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer__page-info">
        <span class="main-footer__copyright"><?= $copyright; ?></span>
        
        <nav class="main-footer__menu" role="navigation" aria-label="<?php _e( 'Footer Menu', 'tangoflow' ); ?>">
        <?php wp_nav_menu( array(
                    'theme_location' => 'footer-menu',
                    'menu_id'        => 'footer-menu',
                    'container'      => '',
                    'fallback_cb'    => 'link_to_menu_editor'
                ) ); ?>
        </nav>
    </div>
</footer>
