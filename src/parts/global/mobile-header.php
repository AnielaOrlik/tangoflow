<?php
    $header_mobile_logo = get_field('logo', 'options')
?>

<div class="mobile-header">
    <button class="mobile-header__close js-menu-close">
        <?= get_image('close'); ?>
    </button>

    <?php if(!empty($header_mobile_logo)) : ?>
        <figure class="mobile-header__logo">
            <?= get_image($header_mobile_logo, 'logo'); ?>
        </figure>
    <?php endif; ?>


    <nav class="mobile-header__menu" role="navigation" aria-label="<?php _e( 'Main Menu', 'tangoflow' ); ?>">
        <?php wp_nav_menu( array(
            'theme_location'     => 'top-menu',
            'container'          => '',
            'menu_class'    => 'mobile-header__menu'
        ) ); ?>
    </nav>

    <div class='mobile-header__language-selector language-selector'>
        <?php language_selector_flags(); ?>
    </div>
</div>
