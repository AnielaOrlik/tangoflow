<?php
/**
 * @package tangoflow
 * @since 1.0
 * @version 1.0
 */

?>

<header class="main-header">
    <div class="container">
        <div class="main-header__menu-wrapper">
            <?php if($logo = get_field('logo', 'options')) : ?>
                <a class="main-header__logo" href="<?= esc_url( home_url( '/' ) ); ?>" rel="home"><?= get_image($logo, 'logo'); ?></a>
            <?php endif; ?>
 
            <nav class="main-header__menu" role="navigation" aria-label="<?php _e( 'Main Menu', 'tangoflow' ); ?>">
                <?php wp_nav_menu( array(
                    'theme_location' => 'top-menu',
                    'menu_id'        => 'top-menu',
                    'container'      => '',
                    'fallback_cb'    => 'link_to_menu_editor'
                ) ); ?>
            </nav>
            <br>
            <div class='main-header__language-selector language-selector'>
                <?php language_selector_flags(); ?>
            </div>
            <button class="nav-trigger js-menu-trigger sticky">
                <?= get_image('menu'); ?>
            </button>
        </div>
    </div>
</header>
