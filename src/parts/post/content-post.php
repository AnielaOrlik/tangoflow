<?php

/**
 * @package tangoflow
 * @since   4.0
 *
 * Shows a single post or page
 * ign_header_block only outputs a header block if the_content does not have a header block.
 * a block is considered a header block if its name starts with header-
 */

?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content container-content grow-font">
			<?php the_content(); ?>
		</div>

		<?php if ( ! is_page() && is_page() ): ?> <!-- Intentionaly false condition to disable code below -->
			<div class="post-entry__post-meta">
				<span class="post-entry__post-meta-date"> <?= get_the_date('Y-m-d'); ?></spna>
				<span class="post-entry__post-meta-author"> by <?= the_author(); ?></spna>
			</div>

			<div class="post-entry__tags">	
				<?php
					$posttags = get_the_tags();
					if ($posttags) { ?>
						<span><?= __('Tags:', 'tangoflow'); ?></span>
						<?php foreach($posttags as $tag) {
							echo $tag->name . ' '; 
						}
					}
				?>
			</div>
		<?php endif; ?>
	</article>


<?php if ( ! is_page() ): ?>
	<section class="after-article container-content">
	<div class="post-entry__pagination">
		<?php
		$prev = get_previous_post();
		if (!empty($prev)) :
		?>
			<a href="<?= get_the_permalink($prev->ID); ?>#tresc" class="post-entry__pagination-link post-entry__pagination-link-prev action-link" title="<?= $prev->post_title; ?>"><?= __('Previous post', 'tangoflow'); ?></a>
		<?php endif; ?>

		<?php
		$next = get_next_post();
		if (!empty($next)) :
		?>
			<a href="<?= get_the_permalink($next->ID); ?>#tresc" class="post-entry__pagination-link post-entry__pagination-link-next action-link" title="<?= $next->post_title; ?>"><?= __('Next post', 'tangoflow'); ?></a>
		<?php endif; ?>
	</div>

		<!-- <?php // If comments are open or we have at least one comment, load up the comment template.
		/* if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif; */
		?> -->
	</section>
<?php
endif;
