<a class="post-card__wrapper" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
	<article class="post-card" id="post-<?php echo get_the_ID(); ?>" <?php post_class( 'card-item' ); ?>>
		<div class="post-card__image col-12 col-md-10 col-lg-5">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail('featured');
			}
			?>
		</div>

		<div class="post-card__content col-12 col-md-10 col-lg-5">

			<h3 class="post-card__title">				
					<?php echo the_title(); ?>				
			</h3>

			<p class="post-card__excerpt">
				<?php the_excerpt(); ?>
			</p>

			<div class="post-card__read">	
						<?= __('read', 'tangoflow'); ?>					
			</div>

			<!-- <div class="card-meta">
				<?php /* ign_posted_on(); ?>
				<?php echo ign_comment_link(); */?>
			</div> -->
		</div>

	</article><!-- #post-## -->
</a>