
import {mobileHeader} from "./core/mobile-header";
import { videoLightbox } from "./core/video-lightbox";
import { smoothScroll } from "./core/smoothScroll";

window.addEventListener("load", () => {
    mobileHeader();
    videoLightbox();
    smoothScroll.init();
});