// const currencyBox = {
//     container: $('.currency-box'),
//     refreshButton: $('.js-currency-refresh'),
//     API: frontEndAjax.proxy + '?type=rates',
//     currentCurrency: $('.currency-box').attr('data-currency'),
//     init() {
//         if (this.container.length) {
//             this.refreshRates();
//             document.addEventListener('currencyChange', (e) => {
//                 console.log('currencyChanged', e);
//                 this.container.find('.currency-name').text(e.detail.currencyName);
//                 this.currentCurrency = e.detail.currencyCode;
//                 this.refreshRates(false);
//             });
//             this.refreshButton.on('click', () => {
//                 this.refreshRates(false);
//             });
//         }
//     },
//     refreshRates(autoRefresh = true) {
//         if (this.container) {
//             this.container.find('.currency-box__price').addClass('loading');
//             fetch(this.API).then(response => response.json()).then(res => {
//                 const data = res[this.currentCurrency];
//                 const rate = (Number(data.Bid) + Number(data.Ask)) / 2;
//                 this.container.find('[data-bid]').text(data.Bid.toFixed(4));
//                 this.container.find('[data-ask]').text(data.Ask.toFixed(4));
//                 this.container.find('[data-rate]').text(rate.toFixed(4));
//                 this.container.find('.currency-box__price').removeClass('loading');

//                 if (autoRefresh) setTimeout(() => { this.refreshRates() }, 5500);
//             })
//         }
//     }
// }




const smoothScroll = {
    init() {
        // Select all links with hashes
        const links = $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')

        links.each(function () {
            $(this).click(function (event) {
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    const target = $(this.hash);
                    console.log(target.length);
                    // target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top - 70
                        }, 1000, function () {
                            // Callback after animation
                            // Must change focus!
                            const $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
        });

    },

    load() {
        if (window.location.hash) {
            const hash = window.location.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top - 70
            }, 1000, 'swing');
        }
    }
}

export {
    smoothScroll
}
    