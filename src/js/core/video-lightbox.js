
export function videoLightbox() {

    const videoLightbox = $('.text-and-video__video-lightbox');
    var src = $('.videowrapper').children('iframe').attr('src');
  
    $('.text-and-video__video-btn, .text-and-video__close-btn').on('click', function() {
      videoLightbox.toggleClass('active');
    });
  
    $('text-and-video__video-btn').click(function(){
      $('.videowrapper').children('iframe').attr('src', src);
    });
  
    $('.text-and-video__close-btn').click(function(){

      $('.videowrapper').children('iframe').attr('src', '');
      location.reload();
    });

  }