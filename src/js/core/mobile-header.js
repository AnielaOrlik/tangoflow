export const mobileHeader = () => {
    const buttonTrigger = document.querySelectorAll('.js-menu-trigger');
    const buttonClose = document.querySelectorAll('.js-menu-close');
    const mobileMenu = document.querySelector('.mobile-header');

    buttonTrigger.forEach( el => el.addEventListener('click', () => {
        console.log('click')
        mobileMenu.classList.toggle('active');
    }));
    buttonClose.forEach( el => el.addEventListener('click', () => {
        mobileMenu.classList.remove('active');
    }));
}