<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #page and #content div and any content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package tangoflow
 * @since 1.0
 * @version 1.0
 */

?>

	</div><!-- #site-content -->
	<?php get_theme_part('global/footer'); ?>
</div><!-- #page -->

</div><!-- .site-container -->

<?php the_field('footer_scripts', 'options'); ?>

<?php wp_footer(); ?>

</body>
</html>
