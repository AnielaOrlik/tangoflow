<?php

/**
* Template Name: Events template
*
*/
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = array('post_type' => 'event', 'posts_per_page' => 6, 'paged' => $paged);
$big = 999999999;

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        <div class="events">
            <div class="container">
                <h2 class='events__page-title'><?php the_title() ?></h2>
                <div class="events__post-grid">
                
                    <?php
                    $loop = new WP_Query( $args ); 
                    
                    while ( $loop->have_posts() ) : $loop->the_post(); 
                        get_theme_part('elements/event-card', ['post_ID' => $post->ID]); 
                    endwhile;

                    ?>

                    <nav class="events__pagination-nav">
                    <?php 
					
					echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $loop->max_num_pages,
                        'prev_text'          => get_image('angle-left') . '<span class="screen-reader-text">' . __('Previous page', 'tangoflow') . '</span>',
                    	'next_text'          => '<span class="screen-reader-text">' . __('Next page', 'tangoflow') . '</span>' . get_image('angle-right'),
                    	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'tangoflow') . ' </span>',
                    ) );
					
                    ?>
                    </nav>
                    <?php
                    wp_reset_postdata(); 
                    ?>
            </div> 
        </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();