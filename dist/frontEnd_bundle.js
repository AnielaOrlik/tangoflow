/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./inc/core/_core.js":
/*!***************************!*\
  !*** ./inc/core/_core.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./inc/scrollmagic/_scrollmagic.js":
/*!*****************************************!*\
  !*** ./inc/scrollmagic/_scrollmagic.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var scrollMagicController = ''; //setup scroller function

/**
 * element can have these data attributes:
 * data-scrollanimation = a class to add to this element on scroll
 * data-scrolltrigger = the element that triggers the scene to start
 * data-scrollhook = onEnter, onLeave, default is center
 * data-scrolloffset = offset from scrollhook on trigger element
 * data-scrollduration = how long it should last. if not set, 0  is used and that means it doesnt reset until you scroll up.
 * data-scrollscrub = tweens between two classes as you scroll. tween expects a duration, else duration will be 100
 *
 */

function runScrollerAttributes(element) {
  //this function can be run on an alement even after load and they will be added to scrollMagicController
  //scrollmagic must be loaded
  if ('undefined' != typeof ScrollMagic && element.hasAttribute('data-scrollanimation')) {
    //scroll animation attributes
    var animationClass = element.dataset.scrollanimation,
        triggerHook = element.dataset.scrollhook || 'center',
        offset = element.dataset.offset || 0,
        triggerElement = element.dataset.scrolltrigger || element,
        duration = element.dataset.duration || 0,
        tween = element.dataset.scrollscrub,
        reverse = element.dataset.reverse || true;
    scene = ''; //if animation has word up or down, its probably an animation that moves it up or down,
    //so make sure trigger element

    if (-1 !== animationClass.toLowerCase().indexOf('up') || -1 !== animationClass.toLowerCase().indexOf('down')) {
      //get parent element and make that the trigger, but use an offset from current element
      if (triggerElement === element) {
        triggerElement = element.parentElement;
        offset = element.offsetTop - triggerElement.offsetTop + parseInt(offset);
      }

      triggerHook = 'onEnter';
    } //if fixed at top, wrap in div


    if (element.getAttribute('data-scrollanimation') === 'fixed-at-top') {
      var wrappedElement = wrap(element, document.createElement('div'));
      wrappedElement.classList.add('fixed-holder');
      triggerHook = 'onLeave';
      triggerElement = element.parentElement;
    } //if scrollscrub exists used tweenmax


    if (tween !== undefined) {
      if (!duration) {
        duration = 100;
      }

      tween = TweenMax.to(element, .65, {
        className: '+=' + animationClass
      }); //finally output the scene

      scene = new ScrollMagic.Scene({
        triggerElement: triggerElement,
        offset: offset,
        triggerHook: triggerHook,
        duration: duration,
        reverse: reverse
      }).setTween(tween).addTo(scrollMagicController) // .addIndicators()
      ;
    } else {
      scene = new ScrollMagic.Scene({
        triggerElement: triggerElement,
        offset: offset,
        triggerHook: triggerHook,
        duration: duration,
        reverse: reverse
      }).on('enter leave', function () {
        //instead of using toggle class we can use these events of on enter and leave and toggle class at both times
        element.classList.toggle(animationClass);
        element.classList.toggle('active'); //if fixed at top set height for spacer and width

        if (element.getAttribute('data-scrollanimation') === 'fixed-at-top') {
          //making fixed item have a set width matching parent
          element.style.width = element.parentElement.clientWidth + 'px';
          element.style.left = element.parentElement.offsetLeft + 'px';
        }
      }).addTo(scrollMagicController) //.setClassToggle(element, animationClass + ' active').addTo(scrollMagicController)
      // .addIndicators()
      ;
    } //good for knowing when its been loaded


    document.body.classList.add('scrollmagic-loaded');
  }
}

document.addEventListener('DOMContentLoaded', function () {
  /*------- Scroll Magic Events Init --------*/
  if ('undefined' != typeof ScrollMagic) {
    scrollMagicController = new ScrollMagic.Controller();
    document.querySelectorAll('[data-scrollanimation]').forEach(function (element) {
      runScrollerAttributes(element);
    });
  }
});

/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/*!**************************************************!*\
  !*** ./node_modules/normalize.css/normalize.css ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var normalize_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! normalize.css */ "./node_modules/normalize.css/normalize.css");
/* harmony import */ var _sass_front_end_bunde_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sass/front-end-bunde.scss */ "./src/sass/front-end-bunde.scss");
/* harmony import */ var _js_core_events__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/core/events */ "./src/js/core/events.js");
/* harmony import */ var _js_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./js/main */ "./src/js/main.js");
!(function webpackMissingModule() { var e = new Error("Cannot find module './js/core/smooth-scroll'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _js_core_responsive_iframe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./js/core/responsive-iframe */ "./src/js/core/responsive-iframe.js");
/* harmony import */ var _inc_core_core_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../inc/core/_core.js */ "./inc/core/_core.js");
/* harmony import */ var _inc_core_core_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_inc_core_core_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _inc_scrollmagic_scrollmagic_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../inc/scrollmagic/_scrollmagic.js */ "./inc/scrollmagic/_scrollmagic.js");
/* harmony import */ var _inc_scrollmagic_scrollmagic_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_inc_scrollmagic_scrollmagic_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _parts_global_browser_update_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./parts/global/_browser_update.js */ "./src/parts/global/_browser_update.js");
/* harmony import */ var _parts_global_browser_update_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_parts_global_browser_update_js__WEBPACK_IMPORTED_MODULE_8__);
 //ADDING SASS
//add your sass files easilt by starting them with an underscore inside the inc or parts folders
// You can also manually add a regular file to the front end bundle so you have access to all scss variables and classes
//adding a separate scss here will work, but you wont have access to scss variables or @use, or @extend

 //js from src




 //add all underscored js files from inc and parts





/***/ }),

/***/ "./src/js/core/events.js":
/*!*******************************!*\
  !*** ./src/js/core/events.js ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _setup__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./setup */ "./src/js/core/setup.js");

/*--------------------------------------------------------------
# Adding some global events and functions users can use via data attributes
--------------------------------------------------------------*/

/**
 * resize menu buttons on load. also runs on resize.
 * menu button is not inside site-top for various reasons (we dont want x to be inside or when menu opens the ex is uinderneath.
 * so we use this function to match the site -top height and center it as if it was inside
 */

var menuButtons = '';

function placeMenuButtons() {
  var $siteTopHeight = document.querySelector('.site-top');

  if ($siteTopHeight != null) {
    $siteTopHeight = $siteTopHeight.clientHeight;
  } // let adminbar = document.querySelector('#wpadminbar');
  // let adminbarHeight = 0;
  //
  // if (adminbar !== null) {
  // 	adminbarHeight = adminbar.clientHeight;
  // }


  if (menuButtons.length) {
    menuButtons.forEach(function (button) {
      button.style.height = $siteTopHeight + 'px';
    });
  }
}
/*--------------------------------------------------------------
# IGN Events
--------------------------------------------------------------*/


document.addEventListener('DOMContentLoaded', function () {
  /*------- Add touch classes or not --------*/
  if (!("ontouchstart" in document.documentElement)) {
    document.documentElement.className += " no-touch-device";
  } else {
    document.documentElement.className += " touch-device";
  }
  /*------- menu buttons --------*/
  //if the menu button is outside site-top. get both buttons for centering both.


  if (!document.querySelector('.app-menu')) {
    menuButtons = document.querySelectorAll('.panel-left-toggle, .panel-right-toggle');
  } else {
    //otherwise the menu button does not need to be centered because its part of the app menu and moves. (moved in navigation.js)
    menuButtons = document.querySelectorAll('.panel-right-toggle');
  } //we run menu button function below in resize event

  /*------- Toggle Buttons --------*/
  //trigger optional afterToggle event
  //adding new custom event for after the element is toggled


  var toggleEvent = null;

  if (isIE11) {
    toggleEvent = document.createEvent('Event'); // Define that the event name is 'build'.

    toggleEvent.initEvent('afterToggle', true, true);
  } else {
    toggleEvent = new Event('afterToggle', {
      bubbles: true
    }); //bubble allows for delegation on body
  } //add aria to buttons currently on page


  var buttons = document.querySelectorAll('[data-toggle]');
  buttons.forEach(function (button) {
    button.setAttribute('role', 'switch');
    button.setAttribute('aria-checked', button.classList.contains('toggled-on') ? 'true' : 'false');
  }); //toggling the buttons with delegation click

  document.body.addEventListener('click', function (e) {
    var item = e.target.closest('[data-toggle]');

    if (item) {
      var $doDefault = item.getAttribute('data-default'); //normally we prevent default unless someone add data-default

      if (null === $doDefault) {
        e.preventDefault();
        e.stopPropagation();
      } //if data-radio is found, only one can be selected at a time.
      // untoggles any other item with same radio value
      //radio items cannot be untoggled until another item is clicked


      var radioSelector = item.getAttribute('data-radio');

      if (radioSelector !== null) {
        var radioSelectors = document.querySelectorAll("[data-radio=\"".concat(radioSelector, "\"]"));
        radioSelectors.forEach(function (radioItem) {
          if (radioItem !== item && radioItem.classList.contains('toggled-on')) {
            toggleItem(radioItem); //toggle all other radio items off when this one is being turned on
          }
        });
      } //if item has data-switch it can only be turned on or off but not both by this button based on value of data-switch (its either on or off)


      var switchItem = item.getAttribute('data-switch'); //finally toggle the clicked item. some types of items cannot be untoggled like radio or an on switch

      if (radioSelector !== null) {
        toggleItem(item, 'on'); //the item clicked on cannot be unclicked until another item is pressed
      } else if (switchItem !== null) {
        if (switchItem === 'on') {
          toggleItem(item, 'on');
        } else {
          toggleItem(item, 'off');
        }
      } else {
        toggleItem(item); //normal regular toggle can turn itself on or off
      }
    } //end if item found

  }); //actual toggle of an item and add class toggled-on and any other classes needed. Also do a slide if necessary

  function toggleItem(item) {
    var forcedState = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';

    //toggle item
    if (forcedState === 'on') {
      item.classList.add('toggled-on'); //radio or data-switch of on will always toggle-on
    } else if (forcedState === 'off') {
      item.classList.remove('toggled-on'); //data-switch of off will always toggle off
    } else {
      item.classList.toggle('toggled-on'); //basic data toggle item
    } //is item toggled? used for the rest of this function to toggle another target if needed.


    var isToggled = item.classList.contains('toggled-on');
    item.setAttribute('aria-expanded', isToggled ? 'true' : 'false'); //get class to add to this item or another

    var $class = item.getAttribute('data-toggle'),
        $target = document.querySelectorAll(item.getAttribute('data-target'));

    if ($class === null || !$class) {
      $class = 'toggled-on'; //default class added is toggled-on
    } //special class added to another item


    if ($target.length) {
      $target.forEach(function (targetItem) {
        if (isToggled) {
          targetItem.classList.add($class);
        } else {
          targetItem.classList.remove($class);
        }

        targetItem.setAttribute('aria-expanded', isToggled ? 'true' : 'false'); //data slide open or closed

        if (targetItem.dataset.slide !== undefined) {
          var slideTime = targetItem.dataset.slide ? parseFloat(targetItem.dataset.slide) : .5;

          if (isToggled) {
            Object(_setup__WEBPACK_IMPORTED_MODULE_0__["ignSlideDown"])(targetItem, slideTime);
          } else {
            ignSlideUp(targetItem, slideTime);
          }
        } //allow event to happen after click for the targeted item


        targetItem.dispatchEvent(toggleEvent);
      });
    } else {
      //applies class to the clicked item, there is no target
      if ($class !== 'toggled-on') {
        //add class to clicked item if its not set to be toggled-on
        if (isToggled) {
          item.classList.toggle($class);
        } else {
          item.classList.remove($class);
        }
      }
    } //trigger optional afterToggle event. continue the click event for customized stuff


    item.dispatchEvent(toggleEvent);
  }
  /*------- Moving items Event as well as all resizing --------*/
  //on Window resize we can move items to and from divs with data-moveto="the destination"
  //it will move there when the site reaches smaller than a size defaulted to 1030 or set that with data-moveat
  //the whole div, including the data att moveto moves back and forth


  var movedId = 0;
  var moveEvent = new Event('afterResize', {
    bubbles: true
  }); //bubble allows for delegation on body

  function moveItems() {
    var windowWidth = window.innerWidth;
    var $moveItems = document.querySelectorAll('[data-moveto]');
    $moveItems.forEach(function (item) {
      var moveAt = item.getAttribute('data-moveat'),
          destination = document.querySelector(item.getAttribute('data-moveto')),
          source = item.getAttribute('data-movefrom');
      moveAt = moveAt ? moveAt : 1030;

      if (moveAt.startsWith('--')) {
        if (isIE11) {
          moveAt = 1030;
        } else {
          var cssVars = getComputedStyle(document.body); //get css variables

          moveAt = parseInt(cssVars.getPropertyValue(moveAt), 10);
        }
      }

      if (!destination) {
        return;
      } //if no data movefrom is found add one to parent so we can move items back in. now they go back and forth


      if (!source) {
        var sourceElem = item.parentElement.id; //if parent has no id attr, add one with a number so its unique

        if (!sourceElem) {
          item.parentElement.setAttribute('id', 'move-' + movedId);
          movedId++;
          sourceElem = item.parentElement.id;
        }

        item.setAttribute('data-movefrom', '#' + sourceElem);
      }

      source = document.querySelector(item.getAttribute('data-movefrom')); //if the screen is smaller than moveAt (1030), move to destination

      if (windowWidth < moveAt || moveAt == 0) {
        //no need to move if its already there...
        if (!destination.contains(item)) {
          if (item.hasAttribute('data-moveto-pos')) {
            destination.insertBefore(item, destination.children[item.getAttribute('data-moveto-pos')]);
          } else {
            destination.appendChild(item);
          }
        }
      } else {
        if (!source.contains(item)) {
          if (item.hasAttribute('data-movefrom-pos')) {
            source.insertBefore(item, source.children[item.getAttribute('data-movefrom-pos')]);
          } else {
            source.appendChild(item);
          }
        }
      } //show it


      item.classList.add('visible');
    });
    placeMenuButtons(); //running the moving of menu buttons here. nothing to do with moving items.
    //fix height of fixed holder fixed at top items

    document.querySelectorAll('.fixed-holder').forEach(function (fixed) {
      fixed.style.height = fixed.firstElementChild.clientHeight + 'px';
    });
    document.dispatchEvent(moveEvent);
  }

  window.addEventListener('resize', Object(_setup__WEBPACK_IMPORTED_MODULE_0__["throttle"])(moveItems, 400));
  moveItems();
  document.documentElement.classList.remove('dom-loading'); //add finished loading ignition events

  var EventFinished = null;

  if (isIE11) {
    EventFinished = document.createEvent('Event'); // Define that the event name is 'build'.

    EventFinished.initEvent('afterIgnEvents', true, true);
  } else {
    EventFinished = new Event('afterIgnEvents');
  }

  document.dispatchEvent(EventFinished);
});
/*------- Function for hi red background image swap --------*/
//check if device is retina

function isHighDensity() {
  return window.matchMedia && window.matchMedia('(-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)').matches;
} //check if file exists on server before using


function fileExists(image_url) {
  var http = new XMLHttpRequest();
  http.open('HEAD', image_url, true);
  http.send();
  return http.status != 404;
} //Add inline retina image if found and on retina device. To use add data-high-res to an inline element with a background-image


if (isHighDensity()) {
  var retinaImage = document.querySelectorAll('[data-high-res]');
  retinaImage.forEach(function (item) {
    var image2x = ''; //if a high res is provided use that, else use background image but add 2x at end.

    if (item.dataset.highRes) {
      image2x = item.dataset.highRes;
    } else {
      //get url for original image
      var image = item.style.backgroundImage.slice(4, -1).replace(/"/g, ""); //add @2x to it if image exists.

      image2x = image.replace(/(\.[^.]+$)/, '@2x$1');
    }

    if (fileExists(image2x)) {
      item.style.backgroundImage = 'url("' + image2x + '")';
    }
  });
}

/***/ }),

/***/ "./src/js/core/mobile-header.js":
/*!**************************************!*\
  !*** ./src/js/core/mobile-header.js ***!
  \**************************************/
/*! exports provided: mobileHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mobileHeader", function() { return mobileHeader; });
var mobileHeader = function mobileHeader() {
  var buttonTrigger = document.querySelectorAll('.js-menu-trigger');
  var buttonClose = document.querySelectorAll('.js-menu-close');
  var mobileMenu = document.querySelector('.mobile-header');
  buttonTrigger.forEach(function (el) {
    return el.addEventListener('click', function () {
      console.log('click');
      mobileMenu.classList.toggle('active');
    });
  });
  buttonClose.forEach(function (el) {
    return el.addEventListener('click', function () {
      mobileMenu.classList.remove('active');
    });
  });
};

/***/ }),

/***/ "./src/js/core/responsive-iframe.js":
/*!******************************************!*\
  !*** ./src/js/core/responsive-iframe.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _setup__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./setup */ "./src/js/core/setup.js");
 //make iframe videos responsive

document.addEventListener('DOMContentLoaded', function () {
  document.querySelectorAll('iframe[src*="youtube.com"], iframe[data-src*="youtube.com"], iframe[src*="vimeo.com"], iframe[data-src*="vimeo.com"]').forEach(function (iframe) {
    if (!iframe.parentElement.classList.contains('videowrapper')) {
      Object(_setup__WEBPACK_IMPORTED_MODULE_0__["wrap"])(iframe).classList.add('videowrapper');
    }
  });
});

/***/ }),

/***/ "./src/js/core/setup.js":
/*!******************************!*\
  !*** ./src/js/core/setup.js ***!
  \******************************/
/*! exports provided: wrap, debounce, throttle, ignSlidePropertyReset, ignSlideUp, ignSlide, ignSlideDown, ignSlideToggle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wrap", function() { return wrap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throttle", function() { return throttle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignSlidePropertyReset", function() { return ignSlidePropertyReset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignSlideUp", function() { return ignSlideUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignSlide", function() { return ignSlide; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignSlideDown", function() { return ignSlideDown; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignSlideToggle", function() { return ignSlideToggle; });
/*------- Core Functions --------*/
//wrap function. use in scrollmagic and more
function wrap(el, wrapper) {
  if (wrapper === undefined) {
    wrapper = document.createElement('div');
  }

  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
  return wrapper;
} //debounce to slow down an event that users window size or the like
//debounce will wait till the window is resized and then run

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
} //throttle will run every few milliseconds as opposed to every millisecond

function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last, deferTimer;
  return function () {
    var context = scope || this;
    var now = +new Date(),
        args = arguments;

    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
} ///slide elements

var ignSlideTimer = Array; //{} //turn into array nad ad a data-sliding wirth a number use that number as index to clear it
//remove inline styling if any found except display

function ignSlidePropertyReset(target, direction) {
  if (direction === 'up') {
    target.style.display = 'none';
  } //clear these properties


  target.style.removeProperty('transition-duration');
  target.style.removeProperty('transition-property');
  target.style.removeProperty('height');
  target.style.removeProperty('padding-top');
  target.style.removeProperty('padding-bottom');
  target.style.removeProperty('margin-top');
  target.style.removeProperty('margin-bottom');
  target.style.removeProperty('overflow');
  target.removeAttribute('slideTimer');
}
function ignSlideUp(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : .5;
  return ignSlide('up', target, duration);
}
function ignSlide() {
  var direction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'up';
  var target = arguments.length > 1 ? arguments[1] : undefined;
  var duration = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : .5;
  return new Promise(function (resolve, reject) {
    if (target.dataset.slideTimer) {
      clearTimeout(parseInt(target.dataset.slideTimer));
      target.removeAttribute('slide-timer');
    }

    var slideTimer = setTimeout(function () {
      ignSlidePropertyReset(target, direction);
      resolve();
    }, duration * 1000);
    target.dataset.slideTimer = slideTimer + ''; //set transitions and overflow

    target.style.transitionProperty = 'height, margin, padding';
    target.style.transitionDuration = duration + 's';

    if (direction === 'up') {
      target.style.overflow = 'hidden'; //no point sliding up if its been set to hidden via css

      if (window.getComputedStyle(target).display === 'none') {
        return;
      } //set height just in case there is none. cannot be nothing or auto


      target.style.height = "".concat(target.scrollHeight, "px"); //1 split second after: closing the height from wherever it is currently

      setTimeout(function () {
        target.style.height = 0; //closing item now

        target.style.paddingTop = 0;
        target.style.paddingBottom = 0;
        target.style.marginBottom = 0;
        target.style.marginTop = 0;
      }, 100);
    } else {
      //sliding down
      // save original margins, and padding, no the inline ones
      var height = window.getComputedStyle(target).height; //might be open... or have a set height

      var display = window.getComputedStyle(target).display;
      var paddingTop = window.getComputedStyle(target).paddingTop || 0;
      var paddingBottom = window.getComputedStyle(target).paddingBottom || 0;
      var marginBottom = window.getComputedStyle(target).marginBottom || 0;
      var marginTop = window.getComputedStyle(target).marginTop || 0;
      target.style.removeProperty('overflow'); //cant animate from auto

      if (height === 'auto') {
        target.style.height = 0;
      } //if its not showing now, we will show from 0 on everything


      if (display === 'none') {
        display = 'block'; //we will be setting this to show

        paddingBottom = paddingTop = marginBottom = marginTop = 0; //animating from 0

        target.style.height = 0;
      } //display must be set before transitioning below


      target.style.display = display; //actual transitions

      setTimeout(function () {
        //animate properties to open and normal
        target.style.height = "".concat(target.scrollHeight, "px"); //also animating the padding and margins

        target.style.paddingTop = paddingTop;
        target.style.paddingBottom = paddingBottom;
        target.style.marginTop = marginTop;
        target.style.marginBottom = marginBottom;
      }, 0);
    }
  });
}
/**
 *
 * @param target
 * @param duration
 *
 * Style element as it should show then set it to display none (or have it get display none from slide up or something else)
 */

function ignSlideDown(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : .5;
  return ignSlide('down', target, duration);
}
function ignSlideToggle(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : .5;

  if (window.getComputedStyle(target).display === 'none') {
    return ignSlideDown(target, duration);
  } else {
    return ignSlideUp(target, duration);
  }
}

/***/ }),

/***/ "./src/js/core/smoothScroll.js":
/*!*************************************!*\
  !*** ./src/js/core/smoothScroll.js ***!
  \*************************************/
/*! exports provided: smoothScroll */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "smoothScroll", function() { return smoothScroll; });
// const currencyBox = {
//     container: $('.currency-box'),
//     refreshButton: $('.js-currency-refresh'),
//     API: frontEndAjax.proxy + '?type=rates',
//     currentCurrency: $('.currency-box').attr('data-currency'),
//     init() {
//         if (this.container.length) {
//             this.refreshRates();
//             document.addEventListener('currencyChange', (e) => {
//                 console.log('currencyChanged', e);
//                 this.container.find('.currency-name').text(e.detail.currencyName);
//                 this.currentCurrency = e.detail.currencyCode;
//                 this.refreshRates(false);
//             });
//             this.refreshButton.on('click', () => {
//                 this.refreshRates(false);
//             });
//         }
//     },
//     refreshRates(autoRefresh = true) {
//         if (this.container) {
//             this.container.find('.currency-box__price').addClass('loading');
//             fetch(this.API).then(response => response.json()).then(res => {
//                 const data = res[this.currentCurrency];
//                 const rate = (Number(data.Bid) + Number(data.Ask)) / 2;
//                 this.container.find('[data-bid]').text(data.Bid.toFixed(4));
//                 this.container.find('[data-ask]').text(data.Ask.toFixed(4));
//                 this.container.find('[data-rate]').text(rate.toFixed(4));
//                 this.container.find('.currency-box__price').removeClass('loading');
//                 if (autoRefresh) setTimeout(() => { this.refreshRates() }, 5500);
//             })
//         }
//     }
// }
var smoothScroll = {
  init: function init() {
    // Select all links with hashes
    var links = $('a[href*="#"]') // Remove links that don't actually link to anything
    .not('[href="#"]').not('[href="#0"]');
    links.each(function () {
      $(this).click(function (event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          // Figure out element to scroll to
          var target = $(this.hash);
          console.log(target.length); // target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?

          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top - 70
            }, 1000, function () {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();

              if ($target.is(":focus")) {
                // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable

                $target.focus(); // Set focus again
              }

              ;
            });
          }
        }
      });
    });
  },
  load: function load() {
    if (window.location.hash) {
      var hash = window.location.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 70
      }, 1000, 'swing');
    }
  }
};


/***/ }),

/***/ "./src/js/core/video-lightbox.js":
/*!***************************************!*\
  !*** ./src/js/core/video-lightbox.js ***!
  \***************************************/
/*! exports provided: videoLightbox */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "videoLightbox", function() { return videoLightbox; });
function videoLightbox() {
  var videoLightbox = $('.text-and-video__video-lightbox');
  var src = $('.videowrapper').children('iframe').attr('src');
  $('.text-and-video__video-btn, .text-and-video__close-btn').on('click', function () {
    videoLightbox.toggleClass('active');
  });
  $('text-and-video__video-btn').click(function () {
    $('.videowrapper').children('iframe').attr('src', src);
  });
  $('.text-and-video__close-btn').click(function () {
    $('.videowrapper').children('iframe').attr('src', '');
    location.reload();
  });
}

/***/ }),

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_mobile_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/mobile-header */ "./src/js/core/mobile-header.js");
/* harmony import */ var _core_video_lightbox__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core/video-lightbox */ "./src/js/core/video-lightbox.js");
/* harmony import */ var _core_smoothScroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./core/smoothScroll */ "./src/js/core/smoothScroll.js");



window.addEventListener("load", function () {
  Object(_core_mobile_header__WEBPACK_IMPORTED_MODULE_0__["mobileHeader"])();
  Object(_core_video_lightbox__WEBPACK_IMPORTED_MODULE_1__["videoLightbox"])();
  _core_smoothScroll__WEBPACK_IMPORTED_MODULE_2__["smoothScroll"].init();
});

/***/ }),

/***/ "./src/parts/global/_browser_update.js":
/*!*********************************************!*\
  !*** ./src/parts/global/_browser_update.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $buoop = {
  required: {
    e: -4,
    f: -3,
    o: -3,
    s: -1,
    c: -3
  },
  insecure: true,
  api: 2020.04
};

function $buo_f() {
  var e = document.createElement("script");
  e.src = "//browser-update.org/update.min.js";
  document.body.appendChild(e);
}

;

try {
  document.addEventListener("DOMContentLoaded", $buo_f, false);
} catch (e) {
  window.attachEvent("onload", $buo_f);
}

/***/ }),

/***/ "./src/sass/front-end-bunde.scss":
/*!***************************************!*\
  !*** ./src/sass/front-end-bunde.scss ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ });
//# sourceMappingURL=frontEnd_bundle.js.map