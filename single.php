<?php
/**
 * The template for displaying all single posts
 * It does not include a sidebar
 *
 * This is the template that displays all posts by default.
 * New post types can use this and the ign_loop will route it to the right folder in template-parts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ignition
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
	        <?php
	        if ( have_posts() ):
		        while ( have_posts() ) : the_post();
			?>
				<div class="container">
					<div class="post-entry">
						<a class="post-entry__link-back" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?= get_image('angle-left')?></a>
						<figure class="post-entry__featured">
							<?php the_post_thumbnail('featured'); ?>
						</figure>
						<h1 class="post-entry__title"><?php the_title(); ?></h1>
						<div class="post-entry__content"><?php  ign_template('content'); ?></div>
					</div>
				</div>

			<?php  endwhile; // End of the loop.
	        endif;
	        ?>
			
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();

