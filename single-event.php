<?php

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
    
            <div class="container">
                <div class="event">
                    
                    <?php
                    if(have_posts()):
                        while(have_posts()) : the_post(); 
                            $return_link = get_field('events_post_grid_link','options'); ?>
                            <a class="post-entry__link-back" href="<?= $return_link; ?>"><?= get_image('angle-left')?></a>
                            <?php the_content(); ?>
                            <div class="event__tags">
                                <?php
                                $posttags = get_the_tags();
                                if ($posttags) { ?>
                                    <span><?= __('Tags:', 'tangoflow'); ?></span>
                                    <?php foreach($posttags as $tag) {
                                        echo $tag->name . ' '; 
                                    }
                                } ?>
                            </div> 
                        <?php
                        endwhile;
                    endif;
                
                        wp_reset_postdata(); 
                    ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();
