<?php

function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a class="wpml-ls-item" href="'.$l['url'].'">';
            else echo '<span class="wpml-ls-item">';
            echo $l['code'];
            if(!$l['active']) echo '</a>';
            else echo '</span>';
        }   
    }
}