<?php 

// register shortcode
add_shortcode('black', 'color_shortcode'); 

// shortcode "black" for text in columns block
function color_shortcode($atts, $content) { 

	return "<span class='text-black'>$content</span>";
} 
// register shortcode
add_shortcode('bold', 'bold_shortcode'); 

// shortcode "bold" for text in columns block
function bold_shortcode($atts, $content) { 

	return "<span class='text-bold'>$content</span>";
} 

// register shortcode
add_shortcode('spacing', 'spacing_shortcode'); 

// shortcode "bold" for text in columns block
function spacing_shortcode($atts, $content) { 

	return "<span class='text-spacing'>$content</span>";
} 


	

