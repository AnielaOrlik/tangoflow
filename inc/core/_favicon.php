<?php
    function add_favicon() {
        $favicon_ID = get_field('favicon', 'options');
        if($favicon_ID) :
            $favicon = wp_get_attachment_url($favicon_ID)
        ?>
            <link rel="icon" type="image/svg+xml" href="<?= $favicon; ?>">
        <?php
        endif;
    }
    add_action('wp_head', 'add_favicon');