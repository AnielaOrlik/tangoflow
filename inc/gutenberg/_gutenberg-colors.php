<?php

function disable_editor_gradients()
{
    add_theme_support('disable-custom-gradients');
    add_theme_support('editor-gradient-presets', array());
}
add_action('after_setup_theme', 'disable_editor_gradients');

add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Blue', 'ea_genesis_child' ),
		'slug'  => 'blue',
		'color'	=> '#59BACC',
	),
	array(
		'name'  => __( 'Green', 'ea_genesis_child' ),
		'slug'  => 'green',
		'color' => '#58AD69',
	),
	array(
		'name'  => __( 'Orange', 'ea_genesis_child' ),
		'slug'  => 'orange',
		'color' => '#FFBC49',
	),
	array(
		'name'	=> __( 'Red', 'ea_genesis_child' ),
		'slug'	=> 'red',
		'color'	=> '#E2574C',
	),
) );