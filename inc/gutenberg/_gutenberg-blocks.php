<?php

add_filter( 'allowed_block_types', 'tangoflow_allowed_block_types' );
 
function tangoflow_allowed_block_types( $allowed_blocks ) {
 
	return array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
        'core/quote',
        'core/gallery',
        'core/columns',
        //acf custom blocks
        'acf/acf-buttons',
        'acf/main-hero',
        'acf/text-and-video',
        'acf/latest-events',
        'acf/image-text',
        'acf/latest-post',
        'acf/teachers',
        'acf/schedule',
        'acf/contact-form',
        'acf/location-map',
        'acf/logo-grid',
        'acf/newsletter-sign-up',
        'acf/plain-text'
	);
 
}